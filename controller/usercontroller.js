const express = require('express');
const router = express.Router();
const ObectId = require('mongoose').Types.ObjectId;
const jwt = require('jsonwebtoken');
const fs = require('fs');
const bcrypt = require('bcrypt');
let User = require('../models/usermodel');
const saltRounds = 10;


router.post('/create',(req,res) => {
    let user = User.find({email:req.body.email},(err,data) => {
        // console.log(data.length);
        if(!err){
            if(data.length > 0){
                res.send('User already exist')
            }else{
                let salt = bcrypt.genSaltSync(saltRounds);
                let hashPassword = bcrypt.hashSync(req.body.password,salt)
                let newUser = new User({
                    "firstname":req.body.firstname,
                    "lastname":req.body.lastname,
                    "username":req.body.username,
                    "email":req.body.email,
                    "password":hashPassword,
                })
                newUser.save();
                let payload = {
                    "firstname":req.body.firstname,
                    "lastname":req.body.lastname,
                    "username":req.body.username,
                    "email":req.body.email,
                    "message":'User created successfully',
                    "status":200
                }
                let token = jwt.sign(payload,'shhhhh');
                let resulvalue = {message : token}
                res.send(resulvalue)
            }
        }else{
            console.log(err);
        }
    })
})

router.post('/login',(req,res)=>{
    let user = User.findOne({email:req.body.email},(err,data)=>{
        if(!err){
            if(data.null !== true){
                console.log(data);
                console.log(`${data.password}`);
                if(bcrypt.compareSync(req.body.password, data.password)==true){
                    let payload = {
                        "firstname":data.firstname,
                        "lastname":data.lastname,
                        "username":data.username,
                        "email":data.email,
                        "message":'User LogIn successfully',
                        "status":200
                    }
                    let token = jwt.sign(payload,'shhhhh');
                    let resulvalue = {message : token}
                    res.send(resulvalue)
                }else{
                    res.status(404).send('please check your email and password')
                }
            }else{
                res.status(404).send('please check you email and password')
            }
        }else{
            res.status(404).send('please check your email and password')
        }
    })
})

router.get('',(req,res) => {
    User.find((error,result) => {
        if(!error){
            res.send(result)
        }else{
            res.send(error)
        }
    })
})


router.get('/:id',(req,res)=>{
    let user = User.findOne({_id:req.params.id},(err,data)=>{
        if(!err){
            if(data!=null){
                res.send(data)
            }else{
                res.status(404).send({message:'Data not found'})
            }
        }else{
            console.log(err);
        }
    })
})

router.put('/update/:id',(req,res)=>{
    User.findOne({_id:req.params.id},(err,data)=>{
        if(!err){
            console.log(data);
            User.updateOne({_id:req.params.id},{$set:req.body},(error,result)=>{
                // console.log(result);
                if(!error){
                    if(result.modifiedCount===1){
                        res.send(`User updated`)
                    }else{
                        res.send(`please try with new value`)
                    }
                }else{
                    res.send(`Already have account`)
                }
            })
        }else{
            res.send('User not exist.')
        }
    })
})

router.delete('/remove/:id',async(req,res)=>{
    let user = await User.deleteOne({_id:req.params.id});
    // console.log(user);
    if(user.deletedCount==1){
        User.deleteOne({_id:req.params.id});
        res.send({status : 200,message : 'Deleted successfully'})
    }else{
        res.status(404).send({status:404,message:'Data not foound'})
    }
})
module.exports = router;