const express = require('express');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors');
const db = require('./db/db');
const swaggerDocument = require('./swagger.json')
let userController = require('./controller/usercontroller');
const basicAuth = require('express-basic-auth');
let app = express();

let options = {
    swaggerOptions:{
        authAction:{JWT:{name:'JWT',schema:{type:'apiKey',in:'header',name:'Authorization',description:''},value:'Bearer<JWT>'}}
    }
}

app.use('/api',basicAuth({authorizer:myAsyncAuthorizer,challenge:true,}),swaggerUi.serve,swaggerUi.setup(swaggerDocument,options));
// app.use('/api',swaggerUi.serve,swaggerUi.setup(swaggerDocument,options));
function myAsyncAuthorizer(username,password){
    const userMatches = basicAuth.safeCompare(username,'admin');
    const passwordMatches = basicAuth.safeCompare(password,'admin@123');
    return userMatches & passwordMatches;
}

app.get('',(req,res)=>{
    res.send('Welcome')
})
app.use(bodyParser.json());



// app.use(cors({origin:true}));
let port = process.env.PORT;
app.listen(port,()=>{
    console.log(`Server listen at port no ${port}`);
});
app.use('/user',userController);