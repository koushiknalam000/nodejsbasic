const mongoose = require('mongoose');
let Users = mongoose.model('Users',{
    firstname : {type : String , required : true},
    lastname : {type : String , required : true},
    username : {type : String , required : true},
    email : {type : String , required : true},
    password : {type : String , required : true},
})
module.exports = Users;